import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios'
import VueAxios from 'vue-axios'
 
Vue.use(Vuex, VueAxios, axios)

export default new Vuex.Store({
  state: {
    products: [],
    userInfo: {
      isLoggedIn: false,
      hasSearched: false,
      productTitleSearched: '',
    },
    credit:0
  },
  

  getters: {
    productsAdded: state => {
      return state.products.filter(el => {
        return el.isAddedToCart;
      });
    },
    getProductById: state => id => {
      return state.products.find(product => product.id === id);
    },
    isUserLoggedIn: state => {
      return state.userInfo.isLoggedIn;
    }
  },
  
  mutations: {
    addCredit: (state, amount) => {
      state.credit = state.credit + parseFloat(amount);
    },
    addProduct: (state, product) => {
      state.products.push(product)
    },
    addToCart: (state, id) => {
      state.products.forEach(el => {
        if (id === el.id) {
          el.isAddedToCart = true;
        }
      });
    },
    setAddedBtn: (state, data) => {
      state.products.forEach(el => {
        if (data.id === el.id) {
          el.isAddedBtn = data.status;
        }
      });
    },
    removeFromCart: (state, id) => {
      state.products.forEach(el => {
        if (id === el.id) {
          el.isAddedToCart = false;
        }
      });
    },
    setIsUserLoggedIn: (state, isUserLoggedIn) => {
      state.userInfo.isLoggedIn = isUserLoggedIn;
    },
    setHasUserSearched: (state, hasSearched) => {
      state.userInfo.hasSearched = hasSearched;
    },
    setProductTitleSearched: (state, titleSearched) => {
      state.userInfo.productTitleSearched = titleSearched;
    },
    productIsAdded(state, payload) {
      state.products.push(payload.product);
    },
    prodcutsIsPatched(state, payload) {
      const { product } = payload;
      const products = state.products.map(product => (product.id === product.id ? product : product));
      Vue.set(state, 'products', products);
    },
    productIsRemoved(state, payload) {
      const product = state.products.filter(product => product.id !== payload.id);
      Vue.set(state, 'products', products);
    },
  },
  
  actions: {
    addPerson(state, product) {
      setTimeout(() => state.commit('personIsAdded', { product }), 500);
    },
    patchPerson(state, products) {
      setTimeout(() => state.commit('personIsPatched', { products }), 500);
    },
    removePerson(state, id) {
      setTimeout(() => state.commit('personIsRemoved', { id }), 500);
    },
  }
});